#pragma once

#include <stdint.h>

typedef uint32_t plong;
typedef uint16_t pword;
typedef uint8_t  pbyte;
